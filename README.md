Exercise brief
Design an API for a vending machine, allowing users with a “seller” role to add, update or remove products, while users with a “buyer” role can deposit coins into the machine and make purchases. Your vending machine should only accept 5, 10, 20, 50 and 100 cent coins
Tasks
•	REST API should be implemented consuming and producing “application/json”
•	Implement product model with amountAvailable, cost, productName and sellerId fields
•	Implement user model with username, password, deposit and role fields
•	Implement CRUD for users (POST shouldn’t require authentication)
•	Implement CRUD for a product model (GET can be called by anyone, while POST, PUT and DELETE can be called only by the seller user who created the product)




ACTIONS
•	Implement /deposit endpoint so users with a “buyer” role can deposit 5, 10, 20, 50 and 100 cent coins into their vending machine account
•	Implement /buy endpoint (accepts productId, amount of products) so users with a “buyer” role can buy products with the money they’ve deposited. API should return total they’ve spent, products they’ve purchased and their change if there’s any (in 5, 10, 20, 50 and 100 cent coins)
•	Implement /reset endpoint so users with a “buyer” role can reset their deposit
•	Take time to think about possible edge cases and access issues that should be solved
Evaluation criteria:
•	Language/Framework of choice best practices
•	Edge cases covered
•	Write API tests for all implemented endpoints
Bonus:
•	Attention to security
Deliverables
A Github repository. Please have the solution running on your computer so the domain expert can tell you which tests to run. Please have a Postman collection ready on your computer so the domain expert can tell you what tests to run on the API.
Miscellaneous
How long do I have to do this?
You should deliver it in 7 days at latest.
What languages should the interface be in?
English only
Who do I contact when I'm done?
The person from Match that initially gave you the exercise link or email us at techchallenge@mvpmatch.co and we will pick it up from there.
Who do I contact if I have questions?
Feel free to use Slack, Discord, Email, Linkedin or carrier pigeon to get in touch with Haris or Sanjin from Match or emails us at techchallenge@mvpmatch.co
Will this code be shown to the client?
Assume yes. Should also help us in reducing the time by clients evaluating profiles.


TEST:
1. create a user with username "user1" and "seller" role
2. create a user with username "user2" and "seller" role
3. create a user with username "user3" and "buyer" role
4. create a product named "product1" with price 70 logged in as user1
5. try creating a product logged in as user3
6. try deleting a product logged in as user2
7. deposit a 100 denomination coin as user3
8. try depositing a 23 denomination coin as user3
9. buy product1 as user3
10. reset deposit as user3
11. try buying product1


