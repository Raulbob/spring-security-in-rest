package com.vending.machine.rfb.VendingMachine;

import com.vending.machine.rfb.VendingMachine.controller.test.TestStuffController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
public class SmokeTest {

    @Autowired
    private TestStuffController testStuffController;

    @Test
    public void contextLoads() {
        assertThat(testStuffController).isNotNull();
    }

}
