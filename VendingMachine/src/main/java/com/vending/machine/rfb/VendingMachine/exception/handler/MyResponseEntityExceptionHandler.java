package com.vending.machine.rfb.VendingMachine.exception.handler;

import com.vending.machine.rfb.VendingMachine.exception.NotEnoughAmountException;
import com.vending.machine.rfb.VendingMachine.exception.ResourceNotFoundException;
import com.vending.machine.rfb.VendingMachine.exception.UnauthorisedDataManipulationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class MyResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = ResourceNotFoundException.class)
    protected ResponseEntity<Object> handle(
            ResourceNotFoundException ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = UnauthorisedDataManipulationException.class)
    protected ResponseEntity<Object> handle(
            UnauthorisedDataManipulationException ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(value = NotEnoughAmountException.class)
    protected ResponseEntity<Object> handle(
            NotEnoughAmountException ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        String bodyOfResponse = "Bad request";
        if (ex.getFieldErrors().size() > 0) {
            StringBuilder stringBuilder = new StringBuilder(bodyOfResponse);
            stringBuilder.append(". Check fields: ");
            for (FieldError fieldError : ex.getFieldErrors()) {
                stringBuilder.append(fieldError.getField()).append(", ");
            }
            bodyOfResponse = stringBuilder.substring(0, stringBuilder.length() - 2);
        }

        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }


}
