package com.vending.machine.rfb.VendingMachine.controller;

import com.vending.machine.rfb.VendingMachine.configuration.MyUserDetails;
import com.vending.machine.rfb.VendingMachine.controller.requests.BuyRequest;
import com.vending.machine.rfb.VendingMachine.controller.requests.DepositRequest;
import com.vending.machine.rfb.VendingMachine.controller.responses.BuyResponse;
import com.vending.machine.rfb.VendingMachine.exception.NotEnoughAmountException;
import com.vending.machine.rfb.VendingMachine.exception.ResourceNotFoundException;
import com.vending.machine.rfb.VendingMachine.exception.UserNotFoundException;
import com.vending.machine.rfb.VendingMachine.model.User;
import com.vending.machine.rfb.VendingMachine.service.ProductService;
import com.vending.machine.rfb.VendingMachine.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class ActionController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    /**
     * Implement /deposit endpoint so users with a “buyer” role can deposit 5, 10, 20, 50 and 100 cent coins into their vending machine account
     */
    @PostMapping("/api/deposit")
    public ResponseEntity<?> depositCoin(@Valid @RequestBody DepositRequest depositRequest) throws UserNotFoundException {
        MyUserDetails principal = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> userOptional = userService.findById(principal.getId());
        if (!userOptional.isPresent()) {
            throw new UserNotFoundException();
        }
        User user = userOptional.get();
        user.addToDeposit(depositRequest.getAmount());
        userService.save(user);
        return ResponseEntity.ok("Amount deposited successfully.");

    }

    /**
     * Implement /buy endpoint (accepts productId, amount of products) so users with a “buyer” role can buy products with the money they’ve deposited.
     * API should return total they’ve spent, products they’ve purchased and their change if there’s any (in 5, 10, 20, 50 and 100 cent coins)
     */
    @PostMapping("/api/buy")
    public ResponseEntity<?> buyProducts(@Valid @RequestBody BuyRequest request) throws ResourceNotFoundException, NotEnoughAmountException {
        MyUserDetails principal = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        BuyResponse buyResponse = productService.buy(request.getProductId(), request.getAmountOfProducts(), principal.getId());
//        return ResponseEntity.ok(new BuyResponse(totalSpending, user.getDeposit(), Collections.singletonList(product)));
        return ResponseEntity.ok(buyResponse);
    }

    @PostMapping("/api/reset")
    public ResponseEntity<?> reset() throws UserNotFoundException {
        MyUserDetails principal = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> userOptional = userService.findById(principal.getId());
        if (!userOptional.isPresent()) {
            throw new UserNotFoundException();
        } else {
            User user = userOptional.get();
            float deposit = user.getDeposit();
            user.setDeposit(0);
            userService.save(user);
            return ResponseEntity.ok("Deposit reset successfully. Returned: " + deposit);
        }
    }

}
