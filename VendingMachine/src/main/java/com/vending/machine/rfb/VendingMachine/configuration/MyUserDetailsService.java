package com.vending.machine.rfb.VendingMachine.configuration;

import com.vending.machine.rfb.VendingMachine.model.User;
import com.vending.machine.rfb.VendingMachine.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> optional = userService.findByUsername(s);
        if (optional.isPresent()) {
            User user = optional.get();
            System.err.println("gasit:" + new MyUserDetails(user));

            return new MyUserDetails(user);
        } else {
            System.err.println("negasit");
            throw new UsernameNotFoundException("Username not found.");
        }
    }

}
