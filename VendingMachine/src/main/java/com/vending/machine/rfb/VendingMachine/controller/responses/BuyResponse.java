package com.vending.machine.rfb.VendingMachine.controller.responses;

import com.vending.machine.rfb.VendingMachine.model.Product;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class BuyResponse {

    private float amountSpent;
    private float change;
    private List<Product> purchasedProducts;

}
