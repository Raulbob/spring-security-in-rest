package com.vending.machine.rfb.VendingMachine;

import com.vending.machine.rfb.VendingMachine.model.User;
import com.vending.machine.rfb.VendingMachine.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VendingMachineApplication implements CommandLineRunner {

	@Autowired
	private UserService userService;

	private static Logger LOG = LoggerFactory
			.getLogger(VendingMachineApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(VendingMachineApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		LOG.info("EXECUTING : command line runner");
		userService.deleteAll();
		User buyer = new User();
		buyer.setEmail("buyer");
		buyer.setPassword("buyer");
		buyer.setRoles("ROLE_BUYER");
		buyer.setDeposit(100);
		userService.save(buyer);

		User seller = new User();
		seller.setEmail("seller");
		seller.setPassword("seller");
		seller.setRoles("ROLE_SELLER");
		seller.setDeposit(0);
		userService.save(seller);

		User seller2 = new User();
		seller2.setEmail("seller2");
		seller2.setPassword("seller");
		seller2.setRoles("ROLE_SELLER");
		seller2.setDeposit(0);
		userService.save(seller2);
	}

}
