package com.vending.machine.rfb.VendingMachine.controller.requests;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreateProductRequest {

    @NotBlank
    private String productName;

    @NotNull
    private Float cost;

    @NotNull
    private Integer amountAvailable;

}
