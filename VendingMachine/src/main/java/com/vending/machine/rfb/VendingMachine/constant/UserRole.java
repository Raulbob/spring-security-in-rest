package com.vending.machine.rfb.VendingMachine.constant;

public class UserRole {

    //admin
    public static final String SELLER = "ROLE_SELLER";

    //user
    public static final String BUYER = "ROLE_BUYER";

}
