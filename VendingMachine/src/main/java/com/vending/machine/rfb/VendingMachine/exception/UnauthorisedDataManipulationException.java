package com.vending.machine.rfb.VendingMachine.exception;

public class UnauthorisedDataManipulationException extends Exception {

    public UnauthorisedDataManipulationException() {
        super("You don not have the rights to this data manipulation method.");
    }

}
