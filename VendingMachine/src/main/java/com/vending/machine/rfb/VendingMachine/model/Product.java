package com.vending.machine.rfb.VendingMachine.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "products")
@Data
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String productName;
    private float cost;
    private int amountAvailable;

    @ManyToOne
    @JoinColumn(name = "seller_id")
    private User user;

    public Product(String productName, float cost, int amountAvailable, User user) {
        this.productName = productName;
        this.cost = cost;
        this.amountAvailable = amountAvailable;
        this.user = user;
    }

}
