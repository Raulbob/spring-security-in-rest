package com.vending.machine.rfb.VendingMachine.exception.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CoinValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface CoinConstraint {
    String message() default "Invalid coin added";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}