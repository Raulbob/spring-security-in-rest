package com.vending.machine.rfb.VendingMachine.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true, prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private MyUserDetailsService userDetailsService;

    /**
     * Authentication.
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("admin")
//                .password("admin")
//                .roles("ADMIN")
//                .and()
//                .withUser("user")
//                .password("user")
//                .roles("USER");
        auth.userDetailsService(userDetailsService);
    }

    /**
     * Authorisation.
     * <p>
     * \/** -> all paths in this level and also nested levels
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable() //we won't use browser for this calls
                .authorizeRequests()  //authorize
                .antMatchers("/api/users").permitAll()
                .antMatchers("/api/products").permitAll()
                .antMatchers("/api/deposit", "/api/buy", "/api/reset").hasRole("BUYER") //actions
                .anyRequest().authenticated() //authenticate
                .and()
                .httpBasic(); //authentication using header
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
