package com.vending.machine.rfb.VendingMachine.controller.test;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Controller for testing security.
 */
@RestController
public class TestStuffController {

    /**
     * Accessed by everyone.
     */
    @GetMapping("/")
    public ResponseEntity<?> home() {
        return new ResponseEntity<>("<h1>Welcome home</h1>", HttpStatus.OK);

    }

    /**
     * Accessed by logged in users (buyer or seller).
     */
    @GetMapping("/buyer")
    public ResponseEntity<?> user() {
        return new ResponseEntity<>("<h1>Welcome home, Buyer</h1>", HttpStatus.OK);

    }

    /**
     * Accessed by logged in users who have the role Seller.
     */
    @GetMapping("/seller")
    public ResponseEntity<?> admin() {
        return new ResponseEntity<>("<h1>Welcome home, Seller</h1>", HttpStatus.OK);

    }


}
