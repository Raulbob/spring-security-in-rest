package com.vending.machine.rfb.VendingMachine.repository;

import com.vending.machine.rfb.VendingMachine.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
