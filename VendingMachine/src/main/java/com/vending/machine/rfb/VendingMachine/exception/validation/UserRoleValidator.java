package com.vending.machine.rfb.VendingMachine.exception.validation;

import com.vending.machine.rfb.VendingMachine.constant.UserRole;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UserRoleValidator implements ConstraintValidator<UserRoleConstraint, String> {


    @Override
    public void initialize(UserRoleConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(String contactField, ConstraintValidatorContext constraintValidatorContext) {
        return contactField.toUpperCase().equals(UserRole.BUYER) || contactField.toUpperCase().equals(UserRole.SELLER);
    }

}
