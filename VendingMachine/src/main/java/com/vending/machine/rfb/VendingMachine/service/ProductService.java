package com.vending.machine.rfb.VendingMachine.service;

import com.vending.machine.rfb.VendingMachine.controller.responses.BuyResponse;
import com.vending.machine.rfb.VendingMachine.exception.NotEnoughAmountException;
import com.vending.machine.rfb.VendingMachine.exception.ResourceNotFoundException;
import com.vending.machine.rfb.VendingMachine.model.Product;
import com.vending.machine.rfb.VendingMachine.model.User;
import com.vending.machine.rfb.VendingMachine.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserService userService;

    public void save(Product user) {
        productRepository.save(user);
    }

    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    public Optional<Product> findById(long id) {
        return productRepository.findById(id);
    }

    public void deleteById(long id) {
        productRepository.deleteById(id);
    }

    /**
     * The principal wants to buy this product
     */
    public BuyResponse buy(long productId, int amountOfProducts, long userId) throws ResourceNotFoundException, NotEnoughAmountException {
        Optional<Product> productOptional = findById(productId);
        Optional<User> userOptional = userService.findById(userId);
        if (!productOptional.isPresent() || !userOptional.isPresent()) {
            throw new ResourceNotFoundException();
        }

        Product product = productOptional.get();
        if (product.getAmountAvailable() - amountOfProducts < 0) {
            throw new NotEnoughAmountException("Not enough products in the vending machine for this operation!");
        }
        product.setAmountAvailable(product.getAmountAvailable() - amountOfProducts);

        User user = userOptional.get();
        float totalSpending = product.getCost() * amountOfProducts;
        if (user.getDeposit() - totalSpending < 0) {
            throw new NotEnoughAmountException("Not enough deposit. Deposit more coins before you can run this operation!");
        }
        user.subtractFromDeposit(totalSpending);
        save(product);
        userService.save(user);

        return new BuyResponse(totalSpending, user.getDeposit(), Collections.singletonList(product));
    }

}
