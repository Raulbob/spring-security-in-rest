package com.vending.machine.rfb.VendingMachine.controller;

import com.vending.machine.rfb.VendingMachine.configuration.MyUserDetails;
import com.vending.machine.rfb.VendingMachine.controller.requests.CreateProductRequest;
import com.vending.machine.rfb.VendingMachine.exception.ProductNotFoundException;
import com.vending.machine.rfb.VendingMachine.exception.UnauthorisedDataManipulationException;
import com.vending.machine.rfb.VendingMachine.exception.UserNotFoundException;
import com.vending.machine.rfb.VendingMachine.model.Product;
import com.vending.machine.rfb.VendingMachine.model.User;
import com.vending.machine.rfb.VendingMachine.service.ProductService;
import com.vending.machine.rfb.VendingMachine.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@PreAuthorize("isAuthenticated()")
public class ProductController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productservice;

    @PreAuthorize("permitAll()")
    @GetMapping("/api/products")
    public ResponseEntity<?> getProduct() {
        return ResponseEntity.ok(productservice.findAll());
    }

    @PreAuthorize("hasRole('ROLE_SELLER')")
    @PostMapping("/api/products")
    public ResponseEntity<?> createProduct(@Valid @RequestBody CreateProductRequest request) throws UserNotFoundException {
        MyUserDetails principal = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> userOptional = userService.findByUsername(principal.getUsername());
        if (!userOptional.isPresent()) {
            throw new UserNotFoundException();
        }
        Product Product = new Product(request.getProductName(), request.getCost(), request.getAmountAvailable(), userOptional.get());
        productservice.save(Product);
        return new ResponseEntity<>("Product created successfully!", HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_SELLER')")
    @PutMapping("/api/products/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable long id, @Valid @RequestBody CreateProductRequest request) throws ProductNotFoundException, UnauthorisedDataManipulationException {
        Optional<Product> productOptional = productservice.findById(id);
        if (!productOptional.isPresent()) {
            throw new ProductNotFoundException();
        }
        Product product = productOptional.get();
        MyUserDetails principal = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!product.getUser().getEmail().equals(principal.getUsername())) {
            throw new UnauthorisedDataManipulationException();
        }
        product.setProductName(request.getProductName());
        product.setCost(request.getCost());
        product.setAmountAvailable(request.getAmountAvailable());
        productservice.save(product);
        return ResponseEntity.ok("Product updated successfully.");

    }

    @PreAuthorize("hasRole('ROLE_SELLER')")
    @DeleteMapping("/api/products/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable long id) throws ProductNotFoundException, UnauthorisedDataManipulationException {
        Optional<Product> productOptional = productservice.findById(id);
        if (!productOptional.isPresent()) {
            throw new ProductNotFoundException();
        }
        Product product = productOptional.get();
        MyUserDetails principal = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!product.getUser().getEmail().equals(principal.getUsername())) {
            throw new UnauthorisedDataManipulationException();
        }

        productservice.deleteById(id);
        return ResponseEntity.ok("Product deleted successfully.");
    }

}
