package com.vending.machine.rfb.VendingMachine.exception.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CoinValidator implements ConstraintValidator<CoinConstraint, Integer> {


    @Override
    public void initialize(CoinConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(Integer contactField, ConstraintValidatorContext constraintValidatorContext) {
        return contactField == 5 || contactField == 10 || contactField == 20 || contactField == 50 || contactField == 100;
    }

}
