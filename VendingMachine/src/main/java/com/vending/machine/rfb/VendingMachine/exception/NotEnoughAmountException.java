package com.vending.machine.rfb.VendingMachine.exception;

public class NotEnoughAmountException extends Exception {

    public NotEnoughAmountException(String message) {
        super(message);
    }

}
