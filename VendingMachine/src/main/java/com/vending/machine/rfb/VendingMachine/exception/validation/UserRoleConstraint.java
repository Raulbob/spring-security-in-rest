package com.vending.machine.rfb.VendingMachine.exception.validation;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UserRoleValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface UserRoleConstraint {
    String message() default "Invalid User Role. Valid: ROLE_SELLER, ROLE_BUYER";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}