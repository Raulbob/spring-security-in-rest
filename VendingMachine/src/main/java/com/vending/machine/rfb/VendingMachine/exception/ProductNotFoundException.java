package com.vending.machine.rfb.VendingMachine.exception;

public class ProductNotFoundException extends ResourceNotFoundException {

    public ProductNotFoundException() {
        super("Product not found!");
    }

}
