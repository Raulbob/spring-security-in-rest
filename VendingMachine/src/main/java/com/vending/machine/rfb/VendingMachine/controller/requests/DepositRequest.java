package com.vending.machine.rfb.VendingMachine.controller.requests;

import com.vending.machine.rfb.VendingMachine.exception.validation.CoinConstraint;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DepositRequest {

    @CoinConstraint
    @NotNull
    private Integer amount;

}
