package com.vending.machine.rfb.VendingMachine.exception;

public class ResourceNotFoundException extends Exception {

    public ResourceNotFoundException() {
        super("Resource not found!");
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }

}
