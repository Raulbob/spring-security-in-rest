package com.vending.machine.rfb.VendingMachine.controller.requests;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class BuyRequest {

    @NotNull
    private Long productId;

    @NotNull
    private Integer amountOfProducts;

}
