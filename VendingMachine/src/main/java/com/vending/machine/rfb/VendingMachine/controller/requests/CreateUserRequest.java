package com.vending.machine.rfb.VendingMachine.controller.requests;

import com.vending.machine.rfb.VendingMachine.exception.validation.UserRoleConstraint;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreateUserRequest {

    @NotBlank(message = "'email' is mandatory")
    private String email;

    @NotBlank
    private String password;

    @UserRoleConstraint
    @NotBlank
    private String userRole;

    @NotNull(message = "Please enter a valid deposit")
    private Float deposit;

}
