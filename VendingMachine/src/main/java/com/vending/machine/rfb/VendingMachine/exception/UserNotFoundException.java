package com.vending.machine.rfb.VendingMachine.exception;

public class UserNotFoundException extends ResourceNotFoundException {

    public UserNotFoundException() {
        super("User not found!");
    }

}
