package com.vending.machine.rfb.VendingMachine.controller;

import com.vending.machine.rfb.VendingMachine.controller.requests.CreateUserRequest;
import com.vending.machine.rfb.VendingMachine.exception.UserNotFoundException;
import com.vending.machine.rfb.VendingMachine.model.User;
import com.vending.machine.rfb.VendingMachine.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@PreAuthorize("isAuthenticated()")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/api/users")
    public ResponseEntity<?> getUser() {
        return ResponseEntity.ok(userService.findAll());
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/api/users")
    public ResponseEntity<?> createUser(@Valid @RequestBody CreateUserRequest request) {
        User user = new User(request.getEmail(), request.getPassword(), request.getUserRole(), request.getDeposit());
        userService.save(user);
        return new ResponseEntity<>("User created successfully!", HttpStatus.CREATED);
    }

    @PutMapping("/api/users/{id}")
    public ResponseEntity<?> updateUser(@PathVariable long id, @Valid @RequestBody CreateUserRequest request) throws UserNotFoundException {
        Optional<User> userOptional = userService.findById(id);
        if (!userOptional.isPresent()) {
            throw new UserNotFoundException();
        }
        User user = userOptional.get();
        user.setRoles(request.getUserRole());
        user.setPassword(request.getPassword());
        user.setEmail(request.getEmail());
        user.setDeposit(request.getDeposit());
        userService.save(user);
        return ResponseEntity.ok("User updated successfully!");
    }

    @DeleteMapping("/api/users/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable long id) throws UserNotFoundException {
        Optional<User> userOptional = userService.findById(id);
        if (!userOptional.isPresent()) {
            throw new UserNotFoundException();
        }
        userService.deleteById(id);
        return ResponseEntity.ok("User deleted successfully!");
    }

}
