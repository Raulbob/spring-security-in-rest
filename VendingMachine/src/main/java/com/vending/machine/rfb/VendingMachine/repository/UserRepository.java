package com.vending.machine.rfb.VendingMachine.repository;

import com.vending.machine.rfb.VendingMachine.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByEmail(String username);

}
