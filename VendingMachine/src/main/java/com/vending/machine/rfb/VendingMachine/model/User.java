package com.vending.machine.rfb.VendingMachine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String email;

    @JsonIgnore
    private String password;
    private String roles;
    private float deposit;

    public User(String email, String password, String roles, float deposit) {
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.deposit = deposit;
    }

    @JsonIgnore
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "seller_id")
    private List<Product> products;

    public void addToDeposit(float amount) {
        deposit += amount;
    }

    public void subtractFromDeposit(float amount) {
        deposit -= amount;
    }

}
